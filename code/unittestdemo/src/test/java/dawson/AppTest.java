package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void testEcho(){
        App.echo(5);
        assertEquals("Testing if the echo method prints the input value", 5, App.echo(5), 0);
    }

    @Test
    public void testOneMore(){
        App.oneMore(5);
        assertEquals("Testing the oneMore method to check if it prints input value +1", 6, App.oneMore(5), 0);
    }
}
